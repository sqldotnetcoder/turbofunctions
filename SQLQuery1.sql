﻿USE [DateFunctions]
GO

DECLARE	@return_value DateTime

EXEC	@return_value = [dbo].[DateCalc]
		@interval = N'd',
		@intervalValue = 2,
		@be = N'b',
		@d = NULL

SELECT	@return_value as 'Return Value'

GO
