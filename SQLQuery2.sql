﻿USE [TurboFunctions]
GO

DECLARE	@return_value DateTime

EXEC	@return_value = [dbo].[DATEPART_TURBO_STR]
		@p = N'd|30|e|10/01/2015'

SELECT	@return_value as 'Return Value'

GO
