﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//This class contains extension methods for the DateTime type
//Extension methods extend the functionality of a class
//The object instance on which the method operates is passed as the first parameter 
//with the "this" keyword
//For example when we call aDate.StartOfMonth(), aDate is passed as parameter dt in 
//method StartOfMonth(this DateTime dt)
//Extension methods must be static.
public static class DateTimeExtensions
{
	//Parameter startOfWeek allows us to pass a starting day different than Sunday 
    public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
    {
        int diff = dt.DayOfWeek - startOfWeek;
        if (diff < 0)
        {
            diff += 7;
        }

        return dt.AddDays(-1 * diff).Date;
    }

    public static DateTime EndOfWeek(this DateTime dt, DayOfWeek startOfWeek)
    {
        DayOfWeek endOfWeek = startOfWeek - 1;
        if (endOfWeek < 0)
            endOfWeek = DayOfWeek.Saturday;

        int diff = endOfWeek - dt.DayOfWeek;
        if (diff < 0)
        {
            diff += 7;
        }

        return dt.AddDays(diff).Date;
    }

    public static DateTime StartOfMonth(this DateTime dt)
    {
		//we use the constructor DateTime(year, month, day)
        return new DateTime(dt.Year, dt.Month, 1);
    }

    public static DateTime EndOfMonth(this DateTime dt)
    {
        DateTime firstDayOfTheMonth = dt.StartOfMonth();
		//This always takes care of 28,29,30,31 days in a month
        return firstDayOfTheMonth.AddMonths(1).AddDays(-1);
    }

    public static DateTime StartOfQuarter(this DateTime dt)
    {
        int quarterNumber = (dt.Month - 1) / 3 + 1;
        return new DateTime(dt.Year, (quarterNumber - 1) * 3 + 1, 1);
    }

    public static DateTime EndOfQuarter(this DateTime dt)
    {
        int quarterNumber = (dt.Month - 1) / 3 + 1;
        DateTime firstDayOfQuarter = new DateTime(dt.Year, (quarterNumber - 1) * 3 + 1, 1);
        return firstDayOfQuarter.AddMonths(3).AddDays(-1);
    }

    public static DateTime StartOfYear(this DateTime dt)
    {
        return new DateTime(dt.Year, 1, 1);
    }

    public static DateTime EndOfYear(this DateTime dt)
    {
        return new DateTime(dt.Year, 12, 31);
    }
}


