﻿-- Examples for queries that exercise different SQL objects implemented by this assembly

-----------------------------------------------------------------------------------------
-- Stored procedure
-----------------------------------------------------------------------------------------
-- exec StoredProcedureName


-----------------------------------------------------------------------------------------
-- User defined function
-----------------------------------------------------------------------------------------
-- select dbo.FunctionName()

select	--dbo.DateCalc('T' , '') Today,
		--dbo.DateCalc('T+1') Tomorrow,
		--dbo.DateCalc('BW-1') PreviousWeekBegin,
		--dbo.DateCalc('EW-1') PreviousWeekEnd,
		dbo.DateCalc('BM+1',  '' ) PreviousMonthBegin 
		--dbo.DateCalc('EM-1') PreviousMonthEnd,
		--dbo.DateCalc('BQ+1') NextQuarterBegin,
		--dbo.DateCalc('EQ+1') NextQuarterEnd,
		--dbo.DateCalc('BY+1') NextYearBegin,
		--dbo.DateCalc('EY+1') NextYearEnd
-----------------------------------------------------------------------------------------
-- User defined type
-----------------------------------------------------------------------------------------
-- CREATE TABLE test_table (col1 UserType)
-- go
--
-- INSERT INTO test_table VALUES (convert(uri, 'Instantiation String 1'))
-- INSERT INTO test_table VALUES (convert(uri, 'Instantiation String 2'))
-- INSERT INTO test_table VALUES (convert(uri, 'Instantiation String 3'))
--
-- select col1::method1() from test_table



-----------------------------------------------------------------------------------------
-- User defined type
-----------------------------------------------------------------------------------------
-- select dbo.AggregateName(Column1) from Table1


select 'To run your project, please edit the Test.sql file in your project. This file is located in the Test Scripts folder in the Solution Explorer.'
