﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
public class DateFunctions
{
    //We set this attribute to indicate that this function 
    //1. does not read any data (DataAccessKind.None)
    //2. may return different result for the same input parameter value (IsDeterministic = false)
    //3. and does not read any data from system tables (SystemDataAccessKind.None)
    [SqlFunction(DataAccess = DataAccessKind.Read, IsDeterministic = false, SystemDataAccess = SystemDataAccessKind.Read)]
    public static SqlDateTime DATEPART_TURBO(
        [SqlFacet(MaxSize = 1)]SqlString interval,
         SqlInt16 intervalValue,
        [SqlFacet(MaxSize = 2)]SqlString be,
        [SqlFacet(IsNullable = true)] SqlDateTime d)
    {

        ArrayList intervalsToValidate = new ArrayList() { "d", "w", "m", "y", "q" };
        ArrayList beToValidate = new ArrayList() { "e", "b" };


        if (be.IsNull)
            throw new ArgumentException("be cannot be NULL");

        ///////////////////////////////////////////////////////////////Validate params
        DateTime baseDate = DateTime.Now.Date;

        ///////////////////////////////////////////////////////////////if the date is not null  then set base date 
        if (d.IsNull)
        {
 
            baseDate = DateTime.Now.Date;
 
        }
        else
        {
            ///////////////////////////////////////////////////////////////if the date is  1900   then treat this situation  as  if NULL 
            if (d.Value.Equals(new DateTime(1900, 1, 1, 0, 0, 0)))
            {
 
                baseDate = DateTime.Now.Date;
 
             }
            else
                baseDate = d.Value;
        }


        if (interval.IsNull)
            throw new ArgumentException("Interval cannot be NULL");

        if (intervalValue.IsNull)
            throw new ArgumentException("Interval value cannot be NULL");


        if (!intervalsToValidate.Contains(interval.Value.ToLower()))
            throw new ArgumentException("interval expression is not valid");

        if (!beToValidate.Contains(be.Value.ToLower()))
            throw new ArgumentException("be value is not valid");
        ///////////////////////////////////////////////////////////////Done with Validate params


        string sbe = be.Value.ToLower();
 
        DateTime retVal = DateTime.Now;

        switch (interval.Value.ToLower())
        {

            case "d":
                retVal = baseDate.AddDays(intervalValue.Value);
                break;
            case "w":
                {
                    retVal = baseDate.AddDays(7 * intervalValue.Value);

                    if (sbe == "b")
                        retVal = retVal.StartOfWeek(DayOfWeek.Sunday);
                    else
                        retVal = retVal.EndOfWeek(DayOfWeek.Sunday);
                    break;
                }
            case "m":
                {
                    retVal = baseDate.AddMonths(intervalValue.Value);

                    if (sbe == "b")
                        retVal = retVal.StartOfMonth();
                    else
                        retVal = retVal.EndOfMonth();
                    break;
                }

            case "q":
                {
                    retVal = baseDate.AddMonths(3 * intervalValue.Value);

                    if (sbe == "b")
                        retVal = retVal.StartOfQuarter();
                    else
                        retVal = retVal.EndOfQuarter();

                    break;
                }
            case "y":
                {
                    retVal = baseDate.AddYears(intervalValue.Value);

                    if (sbe == "b")
                        retVal = retVal.StartOfYear();
                    else
                        retVal = retVal.EndOfYear();

                    break;
                }
            default:
                break;
        }

        if ( baseDate.Date==baseDate  )/////////////////////////////////////////////////////user did not specify a time part
        {
            if (sbe == "e")
            {
                retVal = retVal.Date.AddDays(1).AddSeconds(-1);
            }
           
            
        }
  else
            {
                retVal = retVal.Add(baseDate.TimeOfDay);
            }
        return new SqlDateTime(retVal);


    }


    [SqlFunction(DataAccess = DataAccessKind.Read, IsDeterministic = false, SystemDataAccess = SystemDataAccessKind.Read)]
    public static SqlDateTime DATEPART_TURBO_STR([SqlFacet(MaxSize = 250)] SqlString p)
    {




        if (p.IsNull)
            throw new ArgumentException("parameter cannot be NULL");

        string _p = p.Value.ToLower();

        string[] sa = _p.Split('|');

        if (sa.Length < 3)
        {

            throw new ArgumentException("parameter does not contain enough values");

        }

        string interval = sa[0];
        string intervalvalue = sa[1];

        short iv;

        if (!Int16.TryParse(intervalvalue, out iv))
        {

            throw new ArgumentException("Interval value is not a valid number");
        }


        string be = sa[2];


        //SqlPipe pipe = new SqlPipe();



        //SqlContext.Pipe.Send("test"); 


        DateTime basedate = DateTime.Now.Date;
        if (sa.Length == 4)
        {
            DateTime date;
            if (DateTime.TryParse(sa[3], out  date))
            {
                basedate = date;
            }
            else
            {
                throw new ArgumentException("Date value is not valid");
            }
        }


        return DATEPART_TURBO(interval, iv, be, basedate);






    }
};

